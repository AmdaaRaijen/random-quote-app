import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const QuoteBy = ({ BASE_API_URL }) => {
  const param = useParams();
  const [author, setAuthor] = useState([]);
  const [rotate, setRotate] = useState(false);
  const [author_name, setAuthor_name] = useState("");

  const handleFetch = async () => {
    const res = await fetch(
      `${BASE_API_URL}?author=${
        author_name == "" ? param.author_name : author_name
      }`
    );
    if (res.ok) {
      const data = await res.json();
      setAuthor(data.data);
    }
  };

  const handleFetching = async () => {
    setRotate(true);
    const res = await fetch(`${BASE_API_URL}/random`);
    if (res.ok) {
      const data = await res.json();
      setAuthor_name(data.data[0].quoteAuthor);
      console.log(data.data[0].quoteAuthor);
    }
    setRotate(false);
  };

  useEffect(() => {
    handleFetch();
  }, [author_name]);

  return (
    <div>
      <div
        className="absolute right-[127px] top-[31px] flex flex-row gap-3 hover:cursor-pointer group"
        onClick={handleFetching}
      >
        <p className="font-medium">random</p>
        <div className={rotate ? "animate-spin" : ""}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-6 h-6"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
            />
          </svg>
        </div>
      </div>
      <h1 className="mb-[140px] text-center font-bold">
        {author_name == "" ? param.author_name : author_name}
      </h1>
      <div className="max-w-xl">
        {author.map((e) => {
          return (
            <div className="border-l-4 pl-[99px] border-[#F7DF94] mb-[140px] ">
              <h2 className="text-[36px]">"{e.quoteText}"</h2>
              <br />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default QuoteBy;

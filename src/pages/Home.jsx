import { useEffect, useState } from "react";
import BarLoader from "react-spinners/BarLoader";
import React from "react";
import { Link } from "react-router-dom";

const Home = ({ BASE_API_URL }) => {
  const [quote, setQuote] = useState("");
  const [loading, setLoading] = useState(false);
  const [rotate, setRotate] = useState(false);

  const handleFetching = async () => {
    setLoading(true);
    setRotate(true);
    const res = await fetch(`${BASE_API_URL}/random`);
    if (res.ok) {
      const data = await res.json();
      console.log(data.data);
      setQuote(data.data[0]);
    }
    setLoading(false);
    setRotate(false);
  };

  useEffect(() => {
    handleFetching();
  }, []);

  return (
    <>
      <div
        className="absolute right-[127px] top-[31px] flex flex-row gap-3 hover:cursor-pointer group"
        onClick={handleFetching}
      >
        <p className="font-medium">random</p>
        <div className={rotate ? "animate-spin" : ""}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke-width="1.5"
            stroke="currentColor"
            class="w-6 h-6"
          >
            <path
              stroke-linecap="round"
              stroke-linejoin="round"
              d="M16.023 9.348h4.992v-.001M2.985 19.644v-4.992m0 0h4.992m-4.993 0l3.181 3.183a8.25 8.25 0 0013.803-3.7M4.031 9.865a8.25 8.25 0 0113.803-3.7l3.181 3.182m0-4.991v4.99"
            />
          </svg>
        </div>
      </div>
      <div className="max-w-xl">
        {loading ? (
          <BarLoader />
        ) : (
          <>
            <div className="border-l-4 pl-[99px] border-[#F7DF94]">
              <h1 className="text-[36px]">"{quote.quoteText}"</h1>
            </div>
            <Link
              className="block text-left mt-[159px] p-8 hover:bg-black hover:text-white transition duration-200 cursor-pointer group"
              to={quote.quoteAuthor}
            >
              <div className="flex justify-between items-center">
                <div>
                  <h3 className="font-medium text-[24px]">
                    {quote.quoteAuthor}
                  </h3>
                  <p>{quote.quoteGenre}</p>
                </div>
                <div className="opacity-0 group-hover:opacity-100 ">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke-width="1.5"
                    stroke="currentColor"
                    class="w-6 h-6"
                  >
                    <path
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      d="M17.25 8.25L21 12m0 0l-3.75 3.75M21 12H3"
                    />
                  </svg>
                </div>
              </div>
            </Link>
          </>
        )}
      </div>
    </>
  );
};

export default Home;

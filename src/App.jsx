import Home from "./pages/Home";
import QuoteBy from "./pages/QuoteByAuthor";
import { BrowserRouter, Routes, Route } from "react-router-dom";

const BASE_API_URL = "https://quote-garden.herokuapp.com/api/v3/quotes";

function App() {
  return (
    <>
      <section className="min-h-screen flex items-center justify-center p-10">
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Home BASE_API_URL={BASE_API_URL} />} />
            <Route
              path="/:author_name"
              element={<QuoteBy BASE_API_URL={BASE_API_URL} />}
            />
          </Routes>
        </BrowserRouter>
      </section>
      <footer className="text-center mb-5 font-medium text-slate-500">
        created by{" "}
        <a href="https://github.com/AmdaaRaijen" className="font-bold">
          AmdaaRaijen
        </a>{" "}
        - devchallenges.io
      </footer>
    </>
  );
}

export default App;
